$(document).ready(function() {
		$("#show_playlist").toggle(showPlaylist, showThumbnail, hidePlaylist);
		$('#slide').cycle({
			fx: 'fade',
			after: onAfterSlideChange 
		});

		getThumbnails();
		bindThumbnailHover();
		//bindThumbnailClick();
		bindSongtrackHover();
		loadRdio();
		startRdio('rr93801');
	}
);

function bindSongtrackHover() {
	$(".disc_item.idle .disc_pic").live({
		mouseenter:
			function()
			{
				$(this).animate({opacity: 1, width: '150px', height: '150px', top: '-25px', left: '-25px'}, 350);
			},
		mouseleave:
			function()
			{
				$(this).animate({opacity: 0.5, width: '100px', height: '100px', top: '0px', left: '0px'}, 350);
			}
		});
}

function bindThumbnailHover() {
	$(".thumbnail_pic").live({
		mouseenter:
			function()
			{
				$(this).animate({opacity: 1, width: '150%', left: '-75px'}, 350);
			},
		mouseleave:
			function()
			{
				$(this).animate({opacity: 0.5, width: '100%', left: '0px'}, 350);
			}
	});
}

/*function bindThumbnailClick() {
	$(".thumbnail_pic").live('click', function() {
		
	});
}*/

function showPlaylist() {
	$('#right').show();
	$('#thumbnails').show();
	$('#overlay').fadeIn(0);
	$('#overlay').animate({right: '-135px'}, 350);
}

function hidePlaylist() {
	$('#overlay').animate({right: '-397px', display: 'none'}, 750);
}

function showThumbnail() {
	$('#overlay').animate({right: '0px'}, 200);
}

function getThumbnails() {
	var thumbnailSrc = '';

	$('#slide img').each(function(i) {
		thumbnailSrc = getThumbnail(this.src);
		$('#thumbnails').append(thumbnailSrc);
	});
}

function getThumbnail(src) {
	var html = '<div class="thumbnail">';
	html +=	'<img class="thumbnail_pic idle" src="'+src+'" />';
	html += '</div>';
	return html;
}

function onAfterSlideChange() {
	var slideSrc = $('.thumbnail_pic.active').attr('src');
	if(slideSrc != null) {
		$('.thumbnail_pic.active').parent().remove();
		$('#thumbnails').append(getThumbnail(slideSrc));
	}
	$('#thumbnails .thumbnail_pic.idle').removeAttr('style');
	var slideSrc = $(this)[0].src;
	var thumbnailPic = $('.thumbnail_pic[src="'+slideSrc+'"]')[0];
	if(typeof(thumbnailPic) == 'undefined') return;
	thumbnailPic.className = 'thumbnail_pic active';
}

function loadRdio() {
	var duration = 1; // track the duration of the currently playing track
	$('#rdio').bind('playingTrackChanged.rdio', function(e, playingTrack, sourcePosition) {
		if (playingTrack) {
			duration = playingTrack.duration;
			$('.disc_item.playing img').attr('src', playingTrack.icon);
			$('.disc_item.playing .song_title').text(playingTrack.album + ' - ' + playingTrack.name);
		}
	});

	$('#rdio').bind('positionChanged.rdio', function(e, position) {
		$('#position').css('width', Math.floor(100*position/duration)+'%');
	});

	// this is a valid playback token for localhost.
	// but you should go get your own for your own domain.
	// $('#rdio').rdio('GAlNi78J_____zlyYWs5ZG02N2pkaHlhcWsyOWJtYjkyN2xvY2FsaG9zdEbwl7EHvbylWSWFWYMZwfc=');

	// for snaptunes.co
	$('#rdio').rdio('GAxPprKP_____2R2cHlzNHd5ZXg3Z2M0OXdoaDY3aHdrbnNuYXB0dW5lcy5jb_kBKbaR547VVdYFiT2kopo=');

	$('.disc_item.playing').live('click', function() {
		if($(".disc_item.playing").attr('state') == 'pause'){
			$('#rdio').rdio().play(); 
			$(".disc_item.playing").attr('state', 'playing')
		} else {
			$('#rdio').rdio().pause(); 
			$(".disc_item.playing").attr('state', 'pause')
		}
	});
	
	$('#previous').click(function() { $('#rdio').rdio().previous(); });
	$('#next').click(function() { $('#rdio').rdio().next(); });
}

function startRdio(rdioKey) {
	$('#rdio').bind('ready.rdio', function() {
		$(this).rdio().play(rdioKey);
	});
}
